from setuptools import setup, find_namespace_packages

setup(
    name='communication.radmod',
    version='0.1.0',
    description='Module for radio modulation conversion.',
    author='Invuls',
    packages=find_namespace_packages(),
    package_data={'isf.communication.radmod.resources': ['*', '**/*']},
    zip_safe=False
)
