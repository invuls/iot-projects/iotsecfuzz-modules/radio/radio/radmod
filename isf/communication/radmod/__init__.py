from isf.core import logger


class RadMod:
    bit_array = []
    bit_mode = 8

    def __init__(self, to_encode='31323334', bit_mode=8, little_endian=False,
                 le_bit_block_size=8):

        if type(to_encode) in [list, tuple]:
            logger.info("'to_encode' is bin-array.")
            self.bit_array = to_encode
        else:
            logger.info("'to_encode' is hex-string.")
            hex_decoded = bytes.fromhex(to_encode)
            bits_string = ''.join(
                [bin(c)[2:].zfill(bit_mode) for c in hex_decoded])
            self.bit_array = [int(x) for x in bits_string]

        if little_endian:
            if (len(self.bit_array)) % le_bit_block_size != 1:
                logger.warning(
                    "Little-Endian block size doesn't divide bit length.")
                need_to_add = le_bit_block_size - (
                        (len(self.bit_array)) % le_bit_block_size)
                logger.warning(
                    "Adding {} zero bits at the start of stream.".format(
                        need_to_add))
                self.bit_array = [0] * need_to_add + self.bit_array
                # inverse
                block_array = [self.bit_array[i:i + le_bit_block_size][::-1] for
                               i in
                               range(0, len(self.bit_array),
                                     le_bit_block_size)]
                self.bit_array = []
                for block in block_array:
                    self.bit_array += block
        self.bit_mode = bit_mode
        return

    def bin(self, tostring=False):
        if tostring == 0:
            return self.bit_array
        else:
            return ''.join([str(bit) for bit in self.bit_array])

    def hex(self):
        # array of blocks with length bit_mode
        block_arr = [self.bit_array[i:i + self.bit_mode] for i in
                     range(0, len(self.bit_array), self.bit_mode)]
        # ['11001010','01100101',...]
        bin_str_arr = [''.join([str(c) for c in block]) for block in block_arr]
        result = ''.join(
            [hex(int(bin_block))[2:].zfill(2) for bin_block in bin_str_arr])
        return result

    def oob(self, zerobits_len=1, onebits_len=3, pause_bits=1, tostring=False):
        # 0x30 -> 00110000 -> 0 1000 1000 1110 1110 1000 1000 1000 1000
        full_bit_len = zerobits_len + pause_bits
        if onebits_len > zerobits_len:
            full_bit_len = onebits_len + pause_bits
        oob_bit_arr = [0] * pause_bits
        for bit in self.bit_array:
            if bit:
                oob_bit_arr += [1] * onebits_len + [0] * (
                        full_bit_len - onebits_len)
            else:
                oob_bit_arr += [1] * zerobits_len + [0] * (
                        full_bit_len - zerobits_len)
        if tostring:
            return ''.join([str(bit) for bit in oob_bit_arr])
        else:
            return oob_bit_arr

    def string(self):
        return bytes.fromhex(self.hex()).decode('charmap')

    def nrzi(self, bit_stuff=False, tostring=False, startbit=False,
             change_on_bit=True):
        if len(self.bit_array) < 2:
            logger.error("Stream too short ( < 2 bits )")
            return

        result = [startbit, ]
        count = 0
        for bit_num in range(1, len(self.bit_array)):
            if change_on_bit == self.bit_array[bit_num]:
                result.append(result[-1] ^ True)
                count = 0
            else:
                count += 1

            if bit_stuff and count == 7:
                result[-1] = result[-1] ^ True
                count = 0

        if tostring:
            return ''.join([str(int(x)) for x in result])
        return result
